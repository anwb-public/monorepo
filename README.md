
## Turbo git examples
[Turbo git](https://github.com/vercel/turbo/tree/main/examples/basic)

## Code generation
[code-generation](https://turbo.build/repo/docs/core-concepts/monorepos/code-generation)

1. Nieuwe package aanmaken (Bijvoorbeeld een logo) https://static.anwb.nl/poncho/navigation/images/logo.svg
2. [Manypkg](https://github.com/Thinkmill/manypkg)
3. Compiler ([tsup](https://tsup.egoist.dev/) / [vite](https://vitejs.dev/)) 
4. [Change sets](https://github.com/changesets/changesets)
