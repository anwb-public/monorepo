import { Card } from "@repo/ui/card";
import { Code } from "@repo/ui/code";
import styles from "./page.module.css";
import { Button } from "@repo/ui/button";

const LINKS = [
  {
    title: "title",
    href: "#",
    description: "description",
  },
  {
    title: "title",
    href: "#",
    description: "description",
  },
  {
    title: "title",
    href: "#",
    description: "description",
  },
  {
    title: "title",
    href: "#",
    description: "description",
  },
];

export default function Page(): JSX.Element {
  return (
    <main className={styles.main}>
      <Code className={styles.code}>docs</Code>

      <Button appName="docs" className={styles.button}>
        Click me!
      </Button>

      <div className={styles.grid}>
        {LINKS.map(({ title, href, description }) => (
          <Card className={styles.card} href={href} key={title} title={title}>
            {description}
          </Card>
        ))}
      </div>
    </main>
  );
}
